/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char *my_strcapitalize(char *str)
{
    my_strlowcase(str);

    for (int index = 0; str[index] != '\0'; index++) {
        if (index == 0 && is_alpha(str[index]))
            my_charupcase(str + index);
        if (!is_alpha(str[index - 1])
            && !is_num(str[index - 1]))
            my_charupcase(str + index);
    }

    return str;
}
