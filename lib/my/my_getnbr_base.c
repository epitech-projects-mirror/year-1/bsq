/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_strpos(char const *pattern, char character)
{
    for (int index = 0; *(pattern + index) != '\0'; index++)
        if (*(pattern + index) == character)
            return index;
    return -1;
}

int my_getnbr_base_rec(char const **str, char const *base, int signe)
{
    int base_length = my_strlen(base);
    int index = my_strpos(base, **str);
    int ret = 0;

    while (**str && index < base_length) {
        if (signe == -1)
            ret = ret * base_length - index;
        else
            ret = ret * base_length + index;
        (*str)++;
        index = my_strpos(base, **str);
    }
    return ret;
}

int my_getnbr_base(char const *str, char const *base)
{
    int signe = 1;
    for (; *str == '-' || *str == '+'; str++)
        if (*str == '-')
            signe *= -1;
    return my_getnbr_base_rec(&str, base, signe);
}
