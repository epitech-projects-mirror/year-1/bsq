/*
** EPITECH PROJECT, 2021
** day05
** File description:
** No file there , just an epitech header example
*/

int my_is_prime(int nb)
{
    int index;

    if (nb <= 1)
        return 0;
    for (index = 2; index < nb; index++) {
        if (nb % index == 0)
            return 0;
    }
    return 1;
}
