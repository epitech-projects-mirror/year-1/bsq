/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

char *my_charupcase(char *character)
{
    if (*character >= 'a' && *character <= 'z')
        *character -= 32;
    return character;
}

char *my_strupcase(char *str)
{
    for (int index = 0; *(str + index) != '\0'; index++)
        my_charupcase(str + index);
    return str;
}
