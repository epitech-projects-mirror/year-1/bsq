/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

char *my_charlowcase(char *character)
{
    if (*character >= 'A' && *character <= 'Z')
        *character = (char) (*character + 32);
    return character;
}

char *my_strlowcase(char *str)
{
    for (int index = 0; *(str + index) != '\0'; index++)
        my_charlowcase(str + index);
    return str;
}
