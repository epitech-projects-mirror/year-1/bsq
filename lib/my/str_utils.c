/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int is_alpha(char character)
{
    return ((character >= 'A' && character <= 'Z')
        || (character >= 'a' && character <= 'z'));
}

int is_num(char character)
{
    return (character >= '0' && character <= '9');
}

int is_lowercase(char character)
{
    return (character >= 'a' && character <= 'z');
}

int is_uppercase(char character)
{
    return (character >= 'A' && character <= 'Z');
}
