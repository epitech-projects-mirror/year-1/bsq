/*
** EPITECH PROJECT, 2021
** day07
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char *my_strcat(char *dest, char const *src)
{
    int dest_length = my_strlen(dest);
    for (int index = 0; *(src + index) != '\0'; index++) {
        *(dest + dest_length + index) = *(src + index);
    }
    return dest;
}
