/*
** EPITECH PROJECT, 2021
** day05
** File description:
** No file there , just an epitech header example
*/

int my_compute_square_root(int nb)
{
    int result;
    result = 1;
    while (1) {
        if (result * result == nb)
            return result;
        if (result * result > nb)
            return 0;
        result++;
    }
}
