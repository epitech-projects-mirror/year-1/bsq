/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include <unistd.h>

int my_putchar(char c)
{
    write(1, &c, 1);
    return 0;
}
