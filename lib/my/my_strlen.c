/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int my_strlen(char const *str)
{
    int length = 0;

    while (*(str + length) != '\0')
        length++;
    return length;
}
