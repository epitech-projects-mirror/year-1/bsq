/*
** EPITECH PROJECT, 2021
** day08
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_show_word_array(char *const *tab)
{
    for (int index = 0; tab[index]; index++) {
        my_showstr(tab[index]);
        my_putchar('\n');
    }
    return 0;
}
