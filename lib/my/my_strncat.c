/*
** EPITECH PROJECT, 2021
** day07
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char *my_strncat(char *dest, char const *src, int nb)
{
    if (my_strlen(src) > nb)
        nb = my_strlen(src);
    char source[nb];
    for (int index = 0; index < nb; index++)
        source[index] = src[index];
    return my_strcat(dest, source);
}
