/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int my_strncmp(char const *s1, char const *s2, int n)
{
    int index = 0;
    while (s1[index] == s2[index] && s1[index] != '\0' && s2[index] != '\0') {
        if (index == n)
            break;
        index++;
    }
    return (s1[index] - s2[index]);
}
