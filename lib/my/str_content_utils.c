/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

void fill(char **buffer, int length)
{
    if (length > 0) {
        for (int index = 0; index < length; index++)
            (*buffer)[index] = '\0';
    }
}

void increase_length(char **str, long length)
{
    if (length >= 1) {
        int str_length = my_strlen((*str));
        char *copy = malloc(sizeof(char) * (str_length + 1));

        my_strcpy(copy, (*str));
        copy[str_length] = '\0';
        free((*str));
        (*str) = malloc(sizeof(char) * (str_length + length + 1));
        my_strcpy((*str), copy);
        free(copy);
        for (int index = 0; index < length; index++)
            (*str)[str_length + index] = '\0';
    }
}
