/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int is_printable(char character)
{
    return character >= ' ' && character <= '~';
}

int my_str_isprintable(char const *str)
{
    for (int index = 0; *(str + index) != '\0'; index++) {
        if (!is_printable(*(str + index)))
            return 0;
    }
    return 1;
}
