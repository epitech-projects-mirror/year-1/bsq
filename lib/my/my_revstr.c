/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char *my_revstr(char *str)
{
    int length = my_strlen(str);
    int index = 0;
    int to_change;

    if (length <= 1)
        return str;
    for (to_change = length / 2; to_change > 0; to_change--) {
        char temp = *(str + index);
        *(str + index) = *(str + (length - 1) - index);
        *(str + (length - 1) - index) = temp;
        index++;
    }
    return str;
}
