/*
** EPITECH PROJECT, 2021
** day04
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char is_a_number(char character)
{
    return character >= '0' && character <= '9';
}

int array_to_integer(const char array[], char size, char negative)
{
    long long result = 0;

    for (int index = 0; index < size; index++) {
        long unit = array[index] - NUMBER_BASE;
        long multiple = power(10, size - index - 1);
        result += unit * multiple;
    }

    if (negative && result - 1 > INT_MAX)
        return 0;
    if (!negative && result > INT_MAX)
        return 0;
    return (int) result;
}

int format_result(char result_array[], char result_size, char negative)
{
    int result = array_to_integer(result_array, result_size, negative);
    if (negative)
        result = 0 - result;
    return result;
}

int add_to_array(char const *str, char *array, char *size, char *negative)
{
    if (is_a_number(*str)) {
        array[*size] = *str;
        *(size) += 1;

        if (!is_a_number(*(str + 1)))
            return 1;
    } else if (*str == '-' && is_a_number(*(str + 1)))
        *negative = 1;
    return 0;
}

int my_getnbr(char const *str)
{
    char result_array[my_strlen(str)];
    char result_size = 0;
    char negative;

    while (*str != '\0') {
        if (add_to_array(str, result_array, &result_size, &negative))
            break;
        str++;
    }

    return format_result(result_array, result_size, negative);
}
