/*
** EPITECH PROJECT, 2021
** bistromatic
** File description:
** No file there , just an epitech header example
*/

#include <unistd.h>

void my_puterror(char const *error)
{
    for (int index = 0; error[index] != '\0'; index++) {
        write(2, &(error[index]), 1);
    }
}
