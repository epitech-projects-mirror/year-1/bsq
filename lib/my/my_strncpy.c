/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char *my_strncpy(char *dest, char const *src, int n)
{
    int src_length = my_strlen(src);
    int index = 0;

    while (index < n) {
        if (index == src_length && n > src_length) {
            *(dest + index) = '\0';
            break;
        } else {
            *(dest + index) = *(src + index);
        }
        index++;
    }
    return dest;
}
