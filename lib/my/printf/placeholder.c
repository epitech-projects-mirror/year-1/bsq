/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include <stdlib.h>
#include <stdarg.h>
#include "../my.h"

type *to_type(char identifier, void (*action)(va_list, params))
{
    type *type_data = malloc(sizeof(type));

    type_data->identifier = identifier;
    type_data->action = action;
    return type_data;
}

type **get_types(void)
{
    type **types = malloc(sizeof(type *) * 16);

    types[0] = to_type('%', &print_percentage);
    types[1] = to_type('d', &print_number);
    types[2] = to_type('i', &print_number);
    types[3] = to_type('u', &print_unsigned_number);
    types[4] = to_type('f', &print_double_number);
    types[5] = to_type('F', &print_double_number);
    types[6] = to_type('x', &print_number_hexadecimal);
    types[7] = to_type('X', &print_number_hexadecimal);
    types[8] = to_type('o', &print_number_octal);
    types[9] = to_type('O', &print_number_octal);
    types[10] = to_type('b', &print_number_binary);
    types[11] = to_type('s', &print_string);
    types[12] = to_type('S', &print_printable_string);
    types[13] = to_type('c', &print_character);
    types[14] = to_type('p', &print_address);
    types[15] = to_type(0, 0);
    return types;
}

void free_types(type **types, type const *expect)
{
    int index = 0;

    for (; types[index]->identifier != 0; index++)
        if (expect == 0 || types[index]->identifier != expect->identifier)
            free(types[index]);
    free(types[index]);
}

type *get_type(char identifier)
{
    type **types = get_types();

    for (int index = 0; types[index]->identifier != 0; index++)
        if (types[index]->identifier == identifier) {
            free_types(types, types[index]);
            return types[index];
        }
    free_types(types, 0);
    free(*types);
    return to_type(0, 0);
}

void operate(conststr pattern, int *index, type *ph_type, va_list args)
{
    int length = placeholder_length(pattern, (*index));

    if(length > 0) {
        char *place_holder = extract(pattern, (*index), length);
        params *data = to_params(place_holder, length);
        ph_type = get_type(data->type);

        if (ph_type->action != 0)
            ph_type->action(args, *data);
        (*index) += length;
        free(place_holder);
        free(data);
        free(ph_type);
    } else {
        put_char(pattern[(*index)]);
        (*index)++;
    }
}
