/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include <stdarg.h>
#include "../my.h"

void print_number(va_list args, params data)
{
    int number = va_arg(args, int);

    put_number(number, data);
}

void print_unsigned_number(va_list args, params data)
{
    unsigned int number = va_arg(args, int);

    put_unsigned_number(number, data);
}

void print_double_number(va_list args, params data)
{
    double number = va_arg(args, double);

    put_double(number, data);
}
