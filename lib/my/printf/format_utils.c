/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "../my.h"

void put_n_chars(char character, int amount)
{
    while (amount > 0) {
        put_char(character);
        amount--;
    }
}

int number_length(unsll number)
{
    int length = 0;

    while (number > 0) {
        number /= 10;
        length++;
    }
    return length;
}

void print_raw_number(unsll number)
{
    if (number > 0) {
        int triggered = 0;

        for (unsll base = UNSLL_MAX; base > 0; base /= 10) {
            char nb = (char) (number / base);

            if (nb != 0 || triggered) {
                put_char((char) ('0' + nb));
                triggered = 1;
            }
            number -= nb * base;
        }
    } else {
        put_char('0');
    }
}
