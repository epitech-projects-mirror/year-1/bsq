/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "../my.h"

int power(int base, int power)
{
    long result = 1;

    if (power <= 0)
        return power < 0 ? 0 : 1;
    while (power >= 1) {
        result *= base;
        power--;
    }
    if (result >= 2147483647)
        return 0;
    return ((int) result);
}

void print_number_octal(va_list args, params data)
{
    long number = va_arg(args, long);
    char const *converted = from_decimal(number, 8);
    int length = str_length(converted);

    if (data.raw_data)
        put_char('0');
    if (!data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
    put_string(converted);
    if (data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
}

void print_number_binary(va_list args, params data)
{
    long number = va_arg(args, long);
    char const *converted = from_decimal(number, 2);
    int length = str_length(converted);

    if (!data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
    put_string(converted);
    if (data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
}

void to_upper_case(char *str)
{
    for (int index = 0; str[index] != '\0'; index++) {
        if (str[index] >= 'a' && str[index] <= 'z')
            str[index] = (char) (str[index] - 32);
    }
}

void print_number_hexadecimal(va_list args, params data)
{
    long number = va_arg(args, long);
    char *converted = from_decimal(number, 16);
    int length = str_length(converted);

    if (data.raw_data) {
        put_char('0');
        put_char(data.type == 'X' ? 'X' : 'x');
    }
    if (data.type == 'X')
        to_upper_case(converted);
    if (!data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
    put_string(converted);
    if (data.left_align)
        put_n_chars(data.fill_zeros ? '0' : ' ', data.width - length);
}
