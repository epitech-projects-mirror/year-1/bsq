/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "../my.h"
#include <stdlib.h>

int is_type(char type)
{
    char types[] = {'%', 'd', 'i', 'u', 'f', 'F', 'e', 'E', 'g', 'G',
                    'x', 'X', 'o', 's', 'S', 'c', 'p', 'a', 'A', 'b', 0};
    for (int index = 0; types[index] != 0; index++) {
        if (types[index] == type)
            return 1;
    }
    return 0;
}

int is_flag(char type)
{
    char types[] = {'-', '+', ' ', '0', '\'', '#', 0};

    for (int index = 0; types[index] != 0; index++) {
        if (types[index] == type)
            return 1;
    }
    return 0;
}

int placeholder_length(conststr pattern, int placeholder_index)
{
    int length = 0;

    if (placeholder_index >= 0 && pattern[placeholder_index] == '%') {
        do {
            int total = placeholder_index + (++length);

            if (total >= str_length(pattern) || pattern[total] == '\0')
                return -1;
        } while (!is_type(pattern[placeholder_index + length]));
    }
    return length + 1;
}

int has_width(conststr placeholder, int length)
{
    for (int index = 1; index < length; index++) {
        if (is_flag(placeholder[index]))
            continue;
        if (is_number(placeholder[index]))
            return index;
        if (placeholder[index] == '.')
            return 0;
    }
    return 0;
}

int width_isolation(conststr placeholder, int length)
{
    int width = has_width(placeholder, length);

    if (length > 1 && width) {
        char *number = malloc(sizeof(char));
        int index = 0;

        number[0] = '\0';
        for (; is_number(placeholder[width + index]); index++) {
            increase_length(&number, 1);
            number[index] = placeholder[width + index];
        }
        number[index] = '\0';
        return (get_number(number));
    }
    return -1;
}
