/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "../my.h"
#include <unistd.h>

void put_char(char character)
{
    write(1, &character, 1);
}

void put_unsigned_number(unsll number, params data)
{
    unsll copy = number;
    char replacer = data.fill_zeros ? '0' : ' ';

    if (data.plus_space || data.explicit_plus)
        data.width--;
    if (!data.left_align)
        put_n_chars(replacer, data.width - number_length(number));
    print_raw_number(number);
    if (data.left_align)
        put_n_chars(replacer, data.width - number_length(copy));
}

void put_number(long long number, params data)
{
    if (number < 0)
        put_char('-');
    else if (data.explicit_plus || data.plus_space)
        put_char(data.explicit_plus ? '+' : ' ');
    put_unsigned_number(number >= 0 ? number : -number, data);
}

void put_double(double number, params data)
{
    int integer = (int) number;
    int precision = data.precision;

    put_number(integer, data);
    if (precision < 0)
        precision = 0;
    if (data.raw_data || precision >= 1)
        put_char('.');
    if (precision >= 1) {
        double abs_nbr = (number >= 0 ? number : -number);
        double abs_integer = (integer >= 0 ? integer : -integer);
        int decimal = (int) ((abs_nbr - abs_integer) * power(10, precision));

        put_unsigned_number(decimal, data);
    }
}

void put_string(conststr string)
{
    if (string != 0) {
        for (int index = 0; string[index] != '\0'; index++)
            put_char(string[index]);
    }
}
