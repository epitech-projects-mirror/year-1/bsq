/*
** EPITECH PROJECT, 2021
** my_printf
** File description:
** No file there , just an epitech header example
*/

#include "../my.h"

int array_to_integer(char const array[], char size, char negative)
{
    int result = 0;

    for (int index = 0; index < size; index++) {
        long unit = array[index] - 48;
        long multiple = power(10, size - index - 1);

        result += (int) (unit * multiple);
    }

    if (negative && result - 1 > 2147483647)
        return 0;
    if (!negative && result > 2147483647)
        return 0;
    return result;
}

int format_result(char const result_array[], char result_size, char negative)
{
    int result = array_to_integer(result_array, result_size, negative);

    if (negative)
        result = 0 - result;
    return result;
}

int add_to_array(char const *str, char *array, char *size, char *negative)
{
    if (is_number(*str)) {
        array[*size] = *str;
        *size += 1;

        if (!is_number(*(str + 1)))
            return 1;
    } else if (*str == '-' && is_number(*(str + 1)))
        *negative = 1;
    return 0;
}

int get_number(char const *str)
{
    char result_array[str_length(str)];
    char result_size = 0;
    char negative = 0;

    while (*str != '\0') {
        if (add_to_array(str, result_array, &result_size, &negative))
            break;
        str++;
    }
    return format_result(result_array, result_size, negative);
}
