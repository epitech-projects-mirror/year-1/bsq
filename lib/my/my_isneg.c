/*
** EPITECH PROJECT, 2021
** day03
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_isneg(int n)
{
    if (n < 0)
        my_putchar(78);
    else
        my_putchar(80);
    return 0;
}
