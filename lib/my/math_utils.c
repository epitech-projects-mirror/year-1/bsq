/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int power(int base, int power)
{
    long result = 1;

    if (power < 0)
        return 0;
    if (power == 0)
        return 1;

    while (power >= 1) {
        result *= base;
        power--;
    }

    if (result >= 2147483647)
        return 0;
    return ((int) result);
}
