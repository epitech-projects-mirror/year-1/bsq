/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_str_isupper(char const *str)
{
    for (int index = 0; *(str + index) != '\0'; index++) {
        if (is_alpha(*(str + index))
            && is_lowercase(*(str + index)))
            return 0;
    }
    return 1;
}
