/*
** EPITECH PROJECT, 2021
** day05
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_find_prime_sup(int nb)
{
    while (!my_is_prime(nb))
        nb++;
    return nb;
}
