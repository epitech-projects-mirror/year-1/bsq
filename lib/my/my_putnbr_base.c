/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

void my_putnbr_base_rec(unsigned nb, char *base)
{
    unsigned base_length = my_strlen(base);

    if (nb < base_length) {
        my_putchar(base[nb]);
    } else {
        my_putnbr_base_rec(nb / base_length, base);
        my_putchar(base[nb % base_length]);
    }
}

int my_putnbr_base(int nb, char *base)
{
    if (nb >= 0) {
        my_putnbr_base_rec(nb, base);
    } else {
        my_putchar('-');
        my_putnbr_base_rec(-nb, base);
    }

    return 0;
}
