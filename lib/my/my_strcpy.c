/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

char *my_strcpy(char *dest, char const *src)
{
    int index = 0;
    while (*(src + index) != '\0') {
        *(dest + index) = *(src + index);
        index++;
    }
    *(dest + index) = '\0';
    return dest;
}
