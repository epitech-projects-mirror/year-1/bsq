/*
** EPITECH PROJECT, 2021
** day05
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int check_int_overflow(long value)
{
    if (value >= INT_MAX)
        return 0;
    return ((int) value);
}

int my_compute_power_rec(int base, int power)
{
    if (power < 0) {
        return 0;
    } else if (power == 0) {
        return 1;
    } else {
        long result = base * my_compute_power_rec(base, power - 1);
        return check_int_overflow(result);
    }
}
