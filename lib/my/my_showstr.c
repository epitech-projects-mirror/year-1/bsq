/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

void print_hexa_character(char character)
{
    my_putchar('\\');
    if (character < 16)
        my_putchar('0');
    my_putnbr_base(character, "0123456789abcdef");
}

int my_showstr(char const *str)
{
    for (int index = 0; *(str + index) != '\0'; index++) {
        char character = *(str + index);

        if (is_printable(character))
            my_putchar(character);
        else
            print_hexa_character(character);
    }
    return 0;
}
