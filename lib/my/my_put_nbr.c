/*
** EPITECH PROJECT, 2021
** day03
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_put_nbr(int nb)
{
    if (nb == 0) {
        my_putchar('0');
        return 0;
    }

    if (nb < 0) {
        my_putchar('-');
        nb = 0 - nb;
    }

    int base;
    for (base = 100000000; base > 0; base /= 10) {
        char number = nb / base;
        if (number != 0)
            my_putchar('0' + number);
        nb -= number * base;
    }
    return 0;
}
