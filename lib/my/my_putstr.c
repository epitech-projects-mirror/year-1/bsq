/*
** EPITECH PROJECT, 2021
** day04
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

int my_putstr(char const *str)
{
    const char *start_address;
    start_address = str;
    while (*start_address != '\0') {
        my_putchar(*start_address);
        start_address++;
    }
    return 0;
}
