/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

#include "my.h"

char check_at_index(char *str, char const *to_find, int index)
{
    int i = 0;
    while (*(to_find + i) != '\0') {
        if (*(to_find + i) != *(str + i + index))
            return 0;
        i++;
    }
    return 1;
}

char *my_strstr(char *str, char const *to_find)
{
    int str_length = my_strlen(str);
    int to_find_length = my_strlen(to_find);
    int index = 0;

    while (index < (str_length - to_find_length + 1)) {
        if (check_at_index(str, to_find, index))
            return str + index;
        index++;
    }
    return 0;
}
