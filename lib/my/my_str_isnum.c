/*
** EPITECH PROJECT, 2021
** day06
** File description:
** No file there , just an epitech header example
*/

int my_str_isnum(char const *str)
{
    for (int index = 0; *(str + index) != '\0'; index++) {
        int is_number = (*(str + index) >= '0' && *(str + index) <= '9');
        if (!is_number)
            return 0;
    }
    return 1;
}
