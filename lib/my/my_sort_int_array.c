/*
** EPITECH PROJECT, 2021
** day04
** File description:
** No file there , just an epitech header example
*/

char is_sorted(const int *array, int size)
{
    int index;
    for (index = 0; index < size; index++) {
        if (array[index] > array[index + 1])
            return 0;
    }
    return 1;
}

void sort(int *array, int size)
{
    int temp;
    for (int index = 0; index < size; index++) {
        if (array[index] > array[index + 1]) {
            temp = array[index + 1];
            array[index + 1] = array[index];
            array[index] = temp;
        }
    }
}

void my_sort_int_array(int *array, int size)
{
    while (!is_sorted(array, size)) {
        sort(array, size);
    }
}
