/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#include <stdlib.h>
#include "errors_handling.h"

void check(conststr input)
{
    int first_line = 1;

    for (int index = 0; input[index] != '\0'; index++) {
        char letter = input[index];

        if (letter == '\n' && first_line)
            first_line = 0;
        if (first_line && !is_number(letter)) {
            my_puterror("The first line must only contains lines number\n");
            exit(84);
        }
        if (first_line && is_number(letter))
            continue;
        if (letter != '\n' && letter != '.' && letter != 'o') {
            my_puterror("Invalid character in the file body\n");
            exit(84);
        }
    }
}
