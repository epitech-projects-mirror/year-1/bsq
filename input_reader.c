/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "errors_handling.h"
#include "input_reader.h"
#include "square_finder.h"
#include "str_utils.h"

off_t size_of(conststr file_path)
{
    struct stat buf;

    stat(file_path, &buf);
    return buf.st_size;
}

char *read_input(int file_descriptor, conststr file_path)
{
    int const length = (int) size_of(file_path);
    char *buffer = malloc(sizeof(char) * (length + 1));
    char *input = malloc(sizeof(char));
    long input_length = 0;
    long read_size;

    input[0] = '\0';
    do {
        fill(&buffer, length);
        read_size = read(file_descriptor, buffer, length);
        buffer[length] = '\0';
        increase_length(&input, (read_size + 1));
        my_strcat(input, buffer);
        input_length += read_size;
    } while (read_size >= length);
    free(buffer);
    input[input_length] = '\0';
    check(input);
    return input;
}

board_type format_input(char const *input)
{
    int lines_amount = count_lines(input);
    int lines_length = line_length(input, 1);
    board_type array = malloc(sizeof(char *) * lines_amount);
    int position = 0;
    int line = 0;

    for (int index = 0; index < lines_amount; index++)
        array[index] = malloc(sizeof(unsigned short) * lines_length);
    for (int index = 0; input[index] != '\0'; index++) {
        if (input[index] == '\n') {
            position = 0;
            line++;
        } else if (line >= 1) {
            array[line - 1][position] = (input[index] == '.' ? 1 : 0);
            position++;
        }
    }
    return array;
}

void spread_square(board_type tab, int line, int column)
{
    int save_min = tab[line][column - 1];

    if (tab[line][column] == 0)
        return;
    if (tab[line - 1][column] < save_min)
        save_min = tab[line - 1][column];
    if (tab[line - 1][column - 1] < save_min)
        save_min = tab[line - 1][column - 1];
    if (save_min != 0)
        tab[line][column] = save_min + 1;
}

data_type *analyse_board(board_type board, int height, int width)
{
    data_type *data = malloc(sizeof(data_type));

    data->best_score = 1;
    data->best_line = 0;
    data->best_column = 0;
    if (height > 1 && width > 1) {
        search(board, height, width, data);
    } else {
        find_first(board, height, width, data);
    }
    return data;
}
