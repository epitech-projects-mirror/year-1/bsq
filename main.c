/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "input_reader.h"
#include "str_utils.h"
#include "my.h"

int is_square(int line, int column, data_const data)
{
    return (line <= data->best_line &&
            line > data->best_line - data->best_score &&
            column <= data->best_column &&
            column > data->best_column - data->best_score);
}

void print_board(conststr input)
{
    write(1, input, str_length(input));
}

char *printable_board(board_type board, data_const data, int height, int width)
{
    char *result = malloc(sizeof(char) * ((width + 1) * height + 1));
    int index = 0;

    for (int line = 0; line < height; line++) {
        for (int column = 0; column < width; column++) {
            int can_replace = is_square(line, column, data) && board[line][column] == 1;
            char replace = (char) board[line][column] == 1 ? '.' : 'o';

            result[index] = (char) (can_replace ? 'x' : replace);
            index++;
        }
        result[index] = '\n';
        index++;
    }
    result[(width + 1) * height] = '\0';
    return result;
}

board_type copy_board(board_type board, int height, int width)
{
    board_type array = malloc(sizeof(char *) * height);

    for (int index = 0; index < height; index++)
        array[index] = malloc(sizeof(unsigned short) * width);
    for (int line = 0; line < height; line++) {
        for (int column = 0; column < width; column++) {
            array[line][column] = board[line][column];
        }
    }
    return array;
}

int main(int count, char **args)
{
    if (count == 2) {
        int const file = open(args[1], O_RDONLY);
        if (file != -1) {
            char const *input = read_input(file, args[1]);
            int height = count_lines(input) - 1;
            int width = line_length(input, 1);
            board_type board = format_input(input);
            board_type copy = copy_board(board, height, width);
            data_const data = analyse_board(board, height, width);
            char *board_str = printable_board(copy, data, height, width);

            print_board(board_str);
            free(board_str);
            close(file);
            return 0;
        }
        close(file);
    }
    my_puterror("Please provide a valid filepath\n");
    return 84;
}
