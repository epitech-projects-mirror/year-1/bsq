/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#include "str_utils.h"

int count_lines(char const *str)
{
    int count = 0;

    for (int index = 0; str[index] != '\0'; index++) {
        if (str[index] == '\n' || str[index + 1] == '\0')
            count++;
    }
    return count;
}

int line_length(char const *str, int line)
{
    int count = 0;
    int current_line = 0;

    for (int index = 0; str[index] != '\0'; index++) {
        if (str[index] == '\n')
            current_line++;
        else if (current_line == line)
            count++;
    }
    return count;
}
