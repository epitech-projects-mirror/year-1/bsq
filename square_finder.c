/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#include "square_finder.h"
#include "input_reader.h"

void search_at(board_type board, data_type *data, int line, int column)
{
    if (board[line][column] == 1) {
        data->best_line = line;
        data->best_column = column;
        data->best_score = 1;
        return;
    }
}

void find_first(board_type board, int height, int width, data_type *data)
{
    for (int line = 0; line < height; line++) {
        for (int column = 0; column < width; column++)
            search_at(board, data, line, column);
    }
}

void update_score(board_type board, data_type *data, int line, int column)
{
    if (board[line][column] > data->best_score) {
        data->best_score = board[line][column];
        data->best_line = line;
        data->best_column = column;
    }
}

void search(board_type board, int height, int width, data_type *data)
{
    for (int line = 1; line < height; line++) {
        for (int column = 1; column < width; column++) {
            spread_square(board, line, column);
            update_score(board, data, line, column);
        }
    }
}