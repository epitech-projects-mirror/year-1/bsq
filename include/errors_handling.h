/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#ifndef BSQ_ERRORS_HANDLING_H
    #define BSQ_ERRORS_HANDLING_H

    #include "my.h"

void check(conststr input);

#endif //BSQ_ERRORS_HANDLING_H
