/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#ifndef BSQ_INPUT_READER_H
    #define BSQ_INPUT_READER_H

    #include "my.h"

typedef unsigned short **board_type;

typedef struct data {
    int best_score;
    int best_line;
    int best_column;
} data_type;

typedef data_type const *data_const;

char *read_input(int file_descriptor, conststr file_path);

unsigned short **format_input(char const *input);

data_type *analyse_board(unsigned short **board, int height, int width);

void spread_square(board_type tab, int line, int column);

#endif //BSQ_INPUT_READER_H
