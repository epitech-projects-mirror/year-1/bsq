/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#ifndef BSQ_STR_UTILS_H
    #define BSQ_STR_UTILS_H

void fill(char **buffer, int length);

void increase_length(char **str, long length);

int count_lines(char const *str);

int line_length(char const *str, int line);

#endif //BSQ_STR_UTILS_H
