/*
** EPITECH PROJECT, 2021
** mylib
** File description:
** No file there , just an epitech header example
*/

#ifndef DAY09_MY_H
    #define DAY09_MY_H
    #define NUMBER_BASE 48
    #define INT_MAX 2147483647
    #define UNSLL_MAX 1000000000000000000

    #include <stdarg.h>

typedef unsigned long long unsll;

typedef char const *conststr;

typedef struct params_struct {
    char type;
    int width;
    int precision;
    int left_align;
    int explicit_plus;
    int plus_space;
    int fill_zeros;
    int thousands_separator;
    int raw_data;
} params;

typedef struct type_struct {
    char identifier;

    void (*action)(va_list, params);
} type;


int power(int base, int power);

int my_compute_power_rec(int base, int power);

int my_compute_square_root(int nb);

int my_find_prime_sup(int nb);

char is_a_number(char character);

int my_getnbr(char const *str);

int my_getnbr_base(char const *str, char const *base);

int my_is_prime(int nb);

int my_isneg(int n);

int my_put_nbr(int nb);

int my_putchar(char c);

int my_putnbr_base(int nb, char *base);

int my_putstr(char const *str);

char *my_revstr(char *str);

int my_showmem(char const *str, int size);

int my_showstr(char const *str);

void my_sort_int_array(int *array, int size);

int my_str_isalpha(char const *str);

int my_str_islower(char const *str);

int my_str_isnum(char const *str);

int is_printable(char character);

int my_str_isprintable(char const *str);

int my_str_isupper(char const *str);

char *my_strcapitalize(char *str);

char *my_strcat(char *dest, char const *src);

int my_strcmp(char const *s1, char const *s2);

char *my_strcpy(char *dest, char const *src);

int my_strlen(char const *str);

char *my_charlowcase(char *character);

char *my_strlowcase(char *str);

char *my_strncat(char *dest, char const *src, int nb);

int my_strncmp(char const *s1, char const *s2, int n);

char *my_strncpy(char *dest, char const *src, int n);

char *my_strstr(char *str, char const *to_find);

char *my_strupcase(char *str);

void my_swap(int *a, int *b);

int is_alpha(char character);

int is_num(char character);

int is_lowercase(char character);

int is_uppercase(char character);

int my_show_word_array(char *const *tab);

int check_int_overflow(long value);

int my_getnbr_base_rec(char const **str, char const *base, int signe);

void my_putnbr_base_rec(unsigned nb, char *base);

int my_strpos(char const *pattern, char character);

char is_sorted(const int *array, int size);

char check_at_index(char *str, char const *to_find, int index);

char *my_charupcase(char *character);

void sort(int *array, int size);

void print_hexa_character(char character);

int add_to_array(char const *str, char *array, char *size, char *negative);

char **my_str_to_word_array(char const *str);

char *my_strdup(char const *src);

void my_puterror(char const *error);

void fill(char **buffer, int length);

void increase_length(char **str, long length);

void print_hexa_character(char character);

int add_to_array(char const *str, char *array, char *size, char *negative);

char **my_str_to_word_array(char const *str);

int str_length(conststr str);

char *reverse_str(char *str);

char *from_decimal(long number, int base);

int power(int base, int power);

void print_number_octal(va_list args, params data);

void print_number_binary(va_list args, params data);

void print_number_hexadecimal(va_list args, params data);

void put_n_chars(char character, int amount);

int number_length(unsll number);

void print_raw_number(unsll number);

int my_printf(conststr pattern, ...);

char *str_copy(char *dest, char const *src);

void increase_length(char **str, long length);

int is_number(char character);

int get_number(char const *str);

void print_number(va_list args, params data);

void print_unsigned_number(va_list args, params data);

void print_double_number(va_list args, params data);

type *get_type(char identifier);

int placeholder_length(conststr pattern, int placeholder_index);

int width_isolation(conststr placeholder, int length);

void put_char(char character);

void put_unsigned_number(unsll number, params data);

void put_number(long long number, params data);

void put_double(double number, params data);

void put_string(conststr string);

void print_percentage(va_list args, params data);

void print_string(va_list args, params data);

void print_character(va_list args, params data);

void print_address(va_list args, params data);

void print_printable_string(va_list args, params data);

void operate(conststr pattern, int *index, type *ph_type, va_list args);

char *extract(conststr pattern, int index, int length);

int print_from_index(conststr str, int index);

int contains(conststr str, char character);

params *to_params(conststr placeholder, int length);

#endif //DAY09_MY_H
