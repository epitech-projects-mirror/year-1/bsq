/*
** EPITECH PROJECT, 2021
** bsq
** File description:
** No file there , just an epitech header example
*/

#ifndef BSQ_SQUARE_FINDER_H
    #define BSQ_SQUARE_FINDER_H

    #include "input_reader.h"

void find_first(board_type board, int height, int width, data_type *data);

void search(board_type board, int height, int width, data_type *data);

#endif //BSQ_SQUARE_FINDER_H
