##
## EPITECH PROJECT, 2021
## bsq
## File description:
## No file there , just an epitech header example
##

SRC = ./main.c \
	./str_utils.c \
	./square_finder.c \
	./errors_handling.c \
	./input_reader.c

TESTED_SRC = ./src_utils.c

TESTS_SRC = ./tests/test_format_operations.c

OBJ = $(SRC:.c=.o)

MYLIB = -Iinclude -Llib/my -lmy
CRITERION = -g3 --coverage -lcriterion
NAME = bsq
CFLAGS += -Werror -Wextra -Iinclude

$(NAME): $(OBJ)
	cd lib/my && make
	gcc $(OBJ) -Llib/my -lmy -o./$(NAME)

all: $(NAME)

fclean:
	cd lib/my && make fclean
	make clean
	rm -f $(NAME)
	rm -f $(NAME)_tests

clean:
	find . -type f -name '*.o' -delete
	find . -type f -name '*.gcda' -delete
	find . -type f -name '*.gcno' -delete
	find . -type f -name '*.gcov' -delete

re:
	make fclean
	make

tests_run:
	gcc $(TESTED_SRC) $(TESTS_SRC) $(MYLIB) $(CRITERION) -o./unit-tests
	./unit-tests

coverage:
	make tests_run
	gcovr -e  tests/

gdb:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g
	gdb --quiet ./$(NAME)

debug:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g

valgrind:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g3
	valgrind ./$(NAME) maps/intermediate_map_200_200

leak:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g3
	valgrind --leak-check=full ./$(NAME) maps/intermediate_map_200_200

origins:
	gcc $(SRC) $(MYLIB) -o./$(NAME) -g3
	valgrind --track-origins=yes ./$(NAME) maps/intermediate_map_200_200
